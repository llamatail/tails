# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# Andreas Demmelbauer, 2014
# Christian Spaan <gyges@gmx.net>, 2016
# Claudia <claudianied@web.de>, 2015
# trantor <clucko3@gmail.com>, 2014
# DoKnGH26" 21 <dokngh2621@gmail.com>, 2015
# D P, 2015
# Ettore Atalan <atalanttore@googlemail.com>, 2014-2015
# gerhard <listmember@rinnberger.de>, 2013
# konstibae, 2015
# Larson März <larson@protonmail.ch>, 2013
# Mario Baier <mario.baier26@gmx.de>, 2013
# Mart3000, 2015
# malexmave <inactive+malexmave@transifex.com>, 2014
# max weber, 2015
# Sandra R <drahtseilakt@live.com>, 2014
# Sebastian <sebix+transifex@sebix.at>, 2015
# spriver <tor@dominik-pilatzki.de>, 2015
# sycamoreone <sycamoreone@riseup.net>, 2014
# Tobias Bannert <tobannert@gmail.com>, 2014,2016
# try once, 2015
# rike, 2014
msgid ""
msgstr ""
"Project-Id-Version: The Tor Project\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-03-07 18:07+0100\n"
"PO-Revision-Date: 2016-02-26 16:54+0000\n"
"Last-Translator: Christian Spaan <gyges@gmx.net>\n"
"Language-Team: German (http://www.transifex.com/otf/torproject/language/"
"de/)\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: config/chroot_local-includes/etc/NetworkManager/dispatcher.d/60-tor-ready.sh:39
msgid "Tor is ready"
msgstr "Tor ist bereit"

#: config/chroot_local-includes/etc/NetworkManager/dispatcher.d/60-tor-ready.sh:40
msgid "You can now access the Internet."
msgstr "Sie haben nun Zugriff auf das Internet."

#: config/chroot_local-includes/etc/whisperback/config.py:65
#, python-format
msgid ""
"<h1>Help us fix your bug!</h1>\n"
"<p>Read <a href=\"%s\">our bug reporting instructions</a>.</p>\n"
"<p><strong>Do not include more personal information than\n"
"needed!</strong></p>\n"
"<h2>About giving us an email address</h2>\n"
"<p>\n"
"Giving us an email address allows us to contact you to clarify the problem. "
"This\n"
"is needed for the vast majority of the reports we receive as most reports\n"
"without any contact information are useless. On the other hand it also "
"provides\n"
"an opportunity for eavesdroppers, like your email or Internet provider, to\n"
"confirm that you are using Tails.\n"
"</p>\n"
msgstr ""
"<h1>Helfen uns Ihren Fehler zu beheben!</h1>\n"
"<p>Lesen Sie <a href=\"%s\">unsere Fehlerberichtanleitung</a>.</p>\n"
"<p><strong>Geben Sie nur soviele persönliche Informationen an,\n"
"wie unbedingt notwendig!</strong></p>\n"
"<h2>Über die Angabe von Email-Adressen</h2>\n"
"<p>\n"
"Die Angabe einer Email-Adresse erlaubt uns, Sie zu kontaktieren um das "
"Problem\n"
"zu klären. Dies ist notwendig für die große Mehrheit der erhaltenen "
"Berichte, da die\n"
"meisten Berichte ohne Kontaktinformationen nutzlos sind. Andererseits ist "
"dies eine\n"
"Einladung für böswillige Dritte, wie Ihr Email- oder Internetprovider, um "
"herauszufinden,\n"
"dass Sie Tails nutzen.</p>\n"

#: config/chroot_local-includes/usr/local/bin/electrum:17
msgid "Persistence is disabled for Electrum"
msgstr "Beständiges Speichern ist für Electrum deaktiviert."

#: config/chroot_local-includes/usr/local/bin/electrum:19
msgid ""
"When you reboot Tails, all of Electrum's data will be lost, including your "
"Bitcoin wallet. It is strongly recommended to only run Electrum when its "
"persistence feature is activated."
msgstr ""
"Wenn Sie Tails neu starten, werden alle Daten von Electrum gelöscht, "
"inklusive Ihrer Bitcoin-Geldbörse. Wir empfehlen eindringlich, Electrum mit "
"aktiviertem beständigen Speicherbereich zu nutzen."

#: config/chroot_local-includes/usr/local/bin/electrum:21
msgid "Do you want to start Electrum anyway?"
msgstr "Wollen Sie Electrum trotzdem starten?"

#: config/chroot_local-includes/usr/local/bin/electrum:23
#: config/chroot_local-includes/usr/local/bin/icedove:23
#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:41
msgid "_Launch"
msgstr "_Start"

#: config/chroot_local-includes/usr/local/bin/electrum:24
#: config/chroot_local-includes/usr/local/bin/icedove:24
#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:42
msgid "_Exit"
msgstr "_Beenden"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:136
msgid "OpenPGP encryption applet"
msgstr "OpenPGP-Verschlüsselungs-Applet"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:139
msgid "Exit"
msgstr "Beenden"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:141
msgid "About"
msgstr "Über"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:192
msgid "Encrypt Clipboard with _Passphrase"
msgstr "Zwischenablage mit _Passphrase verschlüsseln"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:195
msgid "Sign/Encrypt Clipboard with Public _Keys"
msgstr "Zwischenablage mit Öffentlichem _Schlüssel signieren/verschlüsseln"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:200
msgid "_Decrypt/Verify Clipboard"
msgstr "Zwischenablage _entschlüsseln/überprüfen"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:204
msgid "_Manage Keys"
msgstr "Schlüssel _verwalten"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:208
msgid "_Open Text Editor"
msgstr "_Textbearbeitung öffnen"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:252
msgid "The clipboard does not contain valid input data."
msgstr "Die Zwischenablage beinhaltet keine gültigen Eingabedaten."

#: config/chroot_local-includes/usr/local/bin/gpgApplet:303
#: config/chroot_local-includes/usr/local/bin/gpgApplet:305
#: config/chroot_local-includes/usr/local/bin/gpgApplet:307
msgid "Unknown Trust"
msgstr "Unbekanntes Vertrauen"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:309
msgid "Marginal Trust"
msgstr "Geringfügiges Vertrauen"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:311
msgid "Full Trust"
msgstr "Volles Vertrauen"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:313
msgid "Ultimate Trust"
msgstr "Unbegrenztes Vertrauen"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:366
msgid "Name"
msgstr "Name"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:367
msgid "Key ID"
msgstr "Schlüsselkennung"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:368
msgid "Status"
msgstr "Status"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:400
msgid "Fingerprint:"
msgstr "Fingerabdruck:"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:403
msgid "User ID:"
msgid_plural "User IDs:"
msgstr[0] "Benutzerkennung"
msgstr[1] "Benutzerkennungen"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:433
msgid "None (Don't sign)"
msgstr "Keine (Nicht signieren)"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:496
msgid "Select recipients:"
msgstr "Empfänger auswählen:"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:504
msgid "Hide recipients"
msgstr "Empfänger verstecken"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:507
msgid ""
"Hide the user IDs of all recipients of an encrypted message. Otherwise "
"anyone that sees the encrypted message can see who the recipients are."
msgstr ""
"Verstecken Sie die Benutzerkennungen von allen Empfängern. Ansonsten weiß "
"jeder, der die verschlüsselte Nachricht sieht, wer die Empfänger sind."

#: config/chroot_local-includes/usr/local/bin/gpgApplet:513
msgid "Sign message as:"
msgstr "Nachricht signieren als:"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:517
msgid "Choose keys"
msgstr "Schlüssel auswählen"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:557
msgid "Do you trust these keys?"
msgstr "Vertrauen Sie diesen Schlüsseln?"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:560
msgid "The following selected key is not fully trusted:"
msgid_plural "The following selected keys are not fully trusted:"
msgstr[0] "Dem folgenden ausgewählten Schlüssel wird nicht komplett vertraut:"
msgstr[1] "Den folgenden ausgewählten Schlüsseln wird nicht komplett vertraut:"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:578
msgid "Do you trust this key enough to use it anyway?"
msgid_plural "Do you trust these keys enough to use them anyway?"
msgstr[0] "Vertrauen Sie diesem Schlüssel genug, um ihn dennoch zu verwenden?"
msgstr[1] "Vertrauen Sie diesen Schlüsseln genug, um sie dennoch zu verwenden?"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:591
msgid "No keys selected"
msgstr "Keine Schlüssel ausgewählt"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:593
msgid ""
"You must select a private key to sign the message, or some public keys to "
"encrypt the message, or both."
msgstr ""
"Sie müssen einen privaten Schlüssel auswählen um die Nachricht zu signieren, "
"oder öffentliche Schlüssel um die Nachricht zu verschlüsseln. Oder beides."

#: config/chroot_local-includes/usr/local/bin/gpgApplet:621
msgid "No keys available"
msgstr "Keine Schlüssel verfügbar"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:623
msgid ""
"You need a private key to sign messages or a public key to encrypt messages."
msgstr ""
"Sie benötigen private Schlüssel, um Nachrichten zu signieren oder einen "
"öffentlichen Schlüssel um Nachrichten zu verschlüsseln."

#: config/chroot_local-includes/usr/local/bin/gpgApplet:751
msgid "GnuPG error"
msgstr "GnuPG-Fehler"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:772
msgid "Therefore the operation cannot be performed."
msgstr "Deshalb kann der Vorgang nicht ausgeführt werden."

#: config/chroot_local-includes/usr/local/bin/gpgApplet:822
msgid "GnuPG results"
msgstr "GnuPG-Ergebnisse"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:828
msgid "Output of GnuPG:"
msgstr "Ausgabe von GnuPG:"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:853
msgid "Other messages provided by GnuPG:"
msgstr "Andere Nachrichten von GnuPG:"

#: config/chroot_local-includes/usr/local/bin/icedove:19
msgid "The <b>Claws Mail</b> persistence feature is activated."
msgstr "Die Persistenzfunktion von <b>Claws Mail</b> ist aktiviert."

#: config/chroot_local-includes/usr/local/bin/icedove:21
msgid ""
"If you have emails saved in <b>Claws Mail</b>, you should <a href='https://"
"tails.boum.org/doc/anonymous_internet/claws_mail_to_icedove'>migrate your "
"data</a> before starting <b>Icedove</b>."
msgstr ""
"Falls Sie gespeicherte E-Mails in <b>Claws Mail</b> haben, sollten Sie noch "
"vor dem Start von <b>Icedove</b> <a href='https://tails.boum.org/doc/"
"anonymous_internet/claws_mail_to_icedove'>Ihre Daten migrieren</a>."

#: config/chroot_local-includes/usr/share/gnome-shell/extensions/shutdown-helper@tails.boum.org/extension.js:71
msgid "Restart"
msgstr "Neustart"

#: config/chroot_local-includes/usr/share/gnome-shell/extensions/shutdown-helper@tails.boum.org/extension.js:74
#: ../config/chroot_local-includes/usr/share/applications/tails-shutdown.desktop.in.h:1
msgid "Power Off"
msgstr "Ausschalten"

#: config/chroot_local-includes/usr/local/bin/tails-about:16
msgid "not available"
msgstr "nicht verfügbar"

#: config/chroot_local-includes/usr/local/bin/tails-about:19
#: ../config/chroot_local-includes/usr/share/desktop-directories/Tails.directory.in.h:1
msgid "Tails"
msgstr "Tails"

#: config/chroot_local-includes/usr/local/bin/tails-about:24
msgid "The Amnesic Incognito Live System"
msgstr "Das Amnesische Inkognito Live System"

#: config/chroot_local-includes/usr/local/bin/tails-about:25
#, python-format
msgid ""
"Build information:\n"
"%s"
msgstr ""
"Versionsinformation:\n"
"%s"

#: config/chroot_local-includes/usr/local/bin/tails-about:27
#: ../config/chroot_local-includes/usr/share/applications/tails-about.desktop.in.h:1
msgid "About Tails"
msgstr "Über Tails"

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:118
#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:124
#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:128
msgid "Your additional software"
msgstr "Ihre zusätzlichen Anwendungen"

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:119
#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:129
msgid ""
"The upgrade failed. This might be due to a network problem. Please check "
"your network connection, try to restart Tails, or read the system log to "
"understand better the problem."
msgstr ""
"Die Aktualisierung ist fehlgeschlagen. Dies könnte an einem Netzwerkproblem "
"liegen. Bitte überprüfen Sie ihre Netzwerkverbindung, starten Sie Tails neu "
"oder lesen Sie die Systemprotokolldateien um das Problem besser zu verstehen."

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:125
msgid "The upgrade was successful."
msgstr "Die Aktualisierung war erfolgreich."

#: config/chroot_local-includes/usr/local/lib/tails-htp-notify-user:52
msgid "Synchronizing the system's clock"
msgstr "Systemuhr wird synchronisiert"

#: config/chroot_local-includes/usr/local/lib/tails-htp-notify-user:53
msgid ""
"Tor needs an accurate clock to work properly, especially for Hidden "
"Services. Please wait..."
msgstr ""
"Tor braucht eine exakte Systemuhr um genau zu arbeiten, besonders für "
"versteckte Dienste. Bitte warten …"

#: config/chroot_local-includes/usr/local/lib/tails-htp-notify-user:87
msgid "Failed to synchronize the clock!"
msgstr "Fehler beim synchronisieren der Systemuhr!"

#: config/chroot_local-includes/usr/local/bin/tails-security-check:146
msgid "This version of Tails has known security issues:"
msgstr "Diese Tails-Version weist Sicherheitslücken auf:"

#: config/chroot_local-includes/usr/local/bin/tails-security-check:156
msgid "Known security issues"
msgstr "Bekannte Sicherheitsprobleme"

#: config/chroot_local-includes/usr/local/lib/tails-spoof-mac:52
#, sh-format
msgid "Network card ${nic} disabled"
msgstr "Netzwerkkarte ${nic} deaktiviert"

#: config/chroot_local-includes/usr/local/lib/tails-spoof-mac:53
#, sh-format
msgid ""
"MAC spoofing failed for network card ${nic_name} (${nic}) so it is "
"temporarily disabled.\n"
"You might prefer to restart Tails and disable MAC spoofing."
msgstr ""
"Bei der Netzwerkkarte ${nic_name} (${nic}) schlug die MAC-Manipulation fehl, "
"sie wurde daher temporär deaktiviert.\n"
"Sie sollten eventuell die MAC-Manipulation deaktivieren und Tails neu "
"starten. "

#: config/chroot_local-includes/usr/local/lib/tails-spoof-mac:62
msgid "All networking disabled"
msgstr "Alle Netzwerke deaktiviert"

#: config/chroot_local-includes/usr/local/lib/tails-spoof-mac:63
#, sh-format
msgid ""
"MAC spoofing failed for network card ${nic_name} (${nic}). The error "
"recovery also failed so all networking is disabled.\n"
"You might prefer to restart Tails and disable MAC spoofing."
msgstr ""
"Bei der Netzwerkkarte ${nic_name} (${nic}) schlugen die MAC-Manipulation und "
"die Fehlerbehebung fehl. Deshalb wurden jetzt alle Verbindungen "
"deaktiviert. \n"
"Sie sollten eventuell die MAC-Manipulation deaktivieren und Tails neu "
"starten."

#: config/chroot_local-includes/usr/local/bin/tails-upgrade-frontend-wrapper:24
#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:27
msgid "error:"
msgstr "Fehler:"

#: config/chroot_local-includes/usr/local/bin/tails-upgrade-frontend-wrapper:25
#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:28
msgid "Error"
msgstr "Fehler"

#: config/chroot_local-includes/usr/local/bin/tails-upgrade-frontend-wrapper:45
msgid ""
"<b>Not enough memory available to check for upgrades.</b>\n"
"\n"
"Make sure this system satisfies the requirements for running Tails.\n"
"See file:///usr/share/doc/tails/website/doc/about/requirements.en.html\n"
"\n"
"Try to restart Tails to check for upgrades again.\n"
"\n"
"Or do a manual upgrade.\n"
"See https://tails.boum.org/doc/first_steps/upgrade#manual"
msgstr ""
"<b>Nicht genügend Speicher vorhanden um nach Aktualisierungen zu suchen.</"
"b>\n"
"\n"
"Stellen Sie bitte sicher, dass dieses System den Minimalanforderungen für "
"Tails entspricht.\n"
"Siehe: file:///usr/share/doc/tails/website/doc/about/requirements.en.html\n"
"\n"
"Versuchen Sie Tails neu zu starten, um erneut nach Aktualisierungen zu "
"suchen.\n"
"\n"
"Oder führen Sie eine manuelle Aktualisierung durch.\n"
"Beachten Sie bitte dazu: https://tails.boum.org/doc/first_steps/"
"upgrade#manual"

#: config/chroot_local-includes/usr/local/lib/tails-virt-notify-user:67
msgid "Warning: virtual machine detected!"
msgstr "Warnung: Virtuelle Maschine erkannt!"

#: config/chroot_local-includes/usr/local/lib/tails-virt-notify-user:69
msgid ""
"Both the host operating system and the virtualization software are able to "
"monitor what you are doing in Tails."
msgstr ""
"Sowohl das Wirtsbetriebssystem als auch die Virtualisierungsanwendung können "
"überwachen, was Sie in Tails machen."

#: config/chroot_local-includes/usr/local/lib/tails-virt-notify-user:72
msgid "Warning: non-free virtual machine detected!"
msgstr "Warnung: nicht-freie virtuelle Maschine erkannt!"

#: config/chroot_local-includes/usr/local/lib/tails-virt-notify-user:74
msgid ""
"Both the host operating system and the virtualization software are able to "
"monitor what you are doing in Tails. Only free software can be considered "
"trustworthy, for both the host operating system and the virtualization "
"software."
msgstr ""
"Sowohl das Wirts-Betriebssystem als auch die Virtualisierungs-Anwendung "
"können überwachen, was Sie in Tails machen. Nur Freie Software kann sowohl "
"für das Wirts-Betriebssystem als auch für die Virtualisierungs-Anwendung als "
"vertrauenswürdig gelten."

#: config/chroot_local-includes/usr/local/lib/tails-virt-notify-user:79
msgid "Learn more"
msgstr "Mehr erfahren"

#: config/chroot_local-includes/usr/local/bin/tor-browser:40
msgid "Tor is not ready"
msgstr "Tor ist nicht bereit"

#: config/chroot_local-includes/usr/local/bin/tor-browser:41
msgid "Tor is not ready. Start Tor Browser anyway?"
msgstr "Tor ist nicht bereit. Möchten Sie den Tor-Browser trotzdem starten?"

#: config/chroot_local-includes/usr/local/bin/tor-browser:42
msgid "Start Tor Browser"
msgstr "Tor-Browser starten"

#: config/chroot_local-includes/usr/local/bin/tor-browser:43
msgid "Cancel"
msgstr "Abbrechen"

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:38
msgid "Do you really want to launch the Unsafe Browser?"
msgstr "Möchten Sie wirklich den unsicheren Browser starten?"

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:40
msgid ""
"Network activity within the Unsafe Browser is <b>not anonymous</b>.\\nOnly "
"use the Unsafe Browser if necessary, for example\\nif you have to login or "
"register to activate your Internet connection."
msgstr ""
"Die Netzwerkaktivität im unsicheren Browser ist <b>nicht anonym</b>. "
"Benutzen Sie den unsicheren Browser nur wenn nötig, z.B. wenn Sie sich "
"einloggen oder registrieren müssen, um Ihre Internetverbindung zu aktivieren."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:52
msgid "Starting the Unsafe Browser..."
msgstr "Unsicherer Browser wird gestartet …"

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:53
msgid "This may take a while, so please be patient."
msgstr "Dies könnte eine Weile dauern, bitte haben Sie etwas Geduld."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:58
msgid "Shutting down the Unsafe Browser..."
msgstr "Unsicherer Browser wird beendet …"

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:59
msgid ""
"This may take a while, and you may not restart the Unsafe Browser until it "
"is properly shut down."
msgstr ""
"Dies könnte eine Weile dauern, und bevor der unsichere Browser geschlossen "
"ist, können Sie ihn nicht noch einmal starten."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:71
msgid "Failed to restart Tor."
msgstr "Tor konnte nicht neu gestartet werden."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:85
#: ../config/chroot_local-includes/usr/share/applications/unsafe-browser.desktop.in.h:1
msgid "Unsafe Browser"
msgstr "Unsicherer Browser"

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:93
msgid ""
"Another Unsafe Browser is currently running, or being cleaned up. Please "
"retry in a while."
msgstr ""
"Ein anderer Unsicherer Browser ist derzeit offen oder wird gerade gesäubert. "
"Bitte versuchen Sie es später noch einmal."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:105
msgid ""
"NetworkManager passed us garbage data when trying to deduce the clearnet DNS "
"server."
msgstr ""
"Beim Versuch den Klarnetz-DNS-Server herauszufinden, hat NetworkManager "
"keine sinnvollen Daten geliefert."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:115
msgid ""
"No DNS server was obtained through DHCP or manually configured in "
"NetworkManager."
msgstr ""
"Konnte keinen DNS-Server über DHCP oder aus den manuellen Einstellungen der "
"Netzwerkverwaltung abrufen."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:123
msgid "Failed to setup chroot."
msgstr "Konnte chroot nicht einrichten."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:129
msgid "Failed to configure browser."
msgstr "Konfiguration des Browses fehlgeschlagen."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:134
msgid "Failed to run browser."
msgstr "Starten des Browsers fehlgeschlagen."

#: config/chroot_local-includes/usr/local/sbin/tails-i2p:34
msgid "I2P failed to start"
msgstr "I2P konnte nicht gestartet werden."

#: config/chroot_local-includes/usr/local/sbin/tails-i2p:35
msgid ""
"Something went wrong when I2P was starting. Check the logs in /var/log/i2p "
"for more information."
msgstr ""
"Etwas ist beim Start von I2P schief gegangen. Überprüfen Sie die Protokolle "
"in var/log/i2p für weitere Informationen."

#: config/chroot_local-includes/usr/local/sbin/tails-i2p:52
msgid "I2P's router console is ready"
msgstr "I2P-Routerkonsole ist bereit"

#: config/chroot_local-includes/usr/local/sbin/tails-i2p:53
msgid "You can now access I2P's router console in the I2P Browser."
msgstr "Sie können nun auf die I2P-Routerkonsole im I2P-Browser zugreifen."

#: config/chroot_local-includes/usr/local/sbin/tails-i2p:58
msgid "I2P is not ready"
msgstr "I2P ist nicht bereit"

#: config/chroot_local-includes/usr/local/sbin/tails-i2p:59
msgid ""
"Eepsite tunnel not built within six minutes. Check the router console in the "
"I2P Browser or the logs in /var/log/i2p for more information. Reconnect to "
"the network to try again."
msgstr ""
"Der Eepsite-Tunnel wurde nicht innerhalb von sechs Minuten aufgebaut. "
"Überprüfen Sie die Routerkonsole im I2P-Browser oder die Protokolle in /var/"
"log/i2p für mehr Informationen. Verbinden Sie sich wieder mit dem Netzwerk, "
"um es erneut zu versuchen."

#: config/chroot_local-includes/usr/local/sbin/tails-i2p:71
msgid "I2P is ready"
msgstr "I2P ist bereit"

#: config/chroot_local-includes/usr/local/sbin/tails-i2p:72
msgid "You can now access services on I2P."
msgstr "Sie können nun auf I2P-Dienste zugreifen."

#: ../config/chroot_local-includes/etc/skel/Desktop/Report_an_error.desktop.in.h:1
msgid "Report an error"
msgstr "Einen Fehler melden"

#: ../config/chroot_local-includes/etc/skel/Desktop/tails-documentation.desktop.in.h:1
#: ../config/chroot_local-includes/usr/share/applications/tails-documentation.desktop.in.h:1
msgid "Tails documentation"
msgstr "Tails-Dokumentation"

#: ../config/chroot_local-includes/usr/share/applications/tails-documentation.desktop.in.h:2
msgid "Learn how to use Tails"
msgstr "Lernen Sie wie Sie Tails benutzen"

#: ../config/chroot_local-includes/usr/share/applications/i2p-browser.desktop.in.h:1
msgid "Anonymous overlay network browser"
msgstr "Anonymer, überlagernder Netzwerkbrowser"

#: ../config/chroot_local-includes/usr/share/applications/i2p-browser.desktop.in.h:2
msgid "I2P Browser"
msgstr "I2P-Browser"

#: ../config/chroot_local-includes/usr/share/applications/tails-about.desktop.in.h:2
msgid "Learn more about Tails"
msgstr "Mehr über Tails erfahren"

#: ../config/chroot_local-includes/usr/share/applications/tails-reboot.desktop.in.h:1
msgid "Reboot"
msgstr "Neustart"

#: ../config/chroot_local-includes/usr/share/applications/tails-reboot.desktop.in.h:2
msgid "Immediately reboot computer"
msgstr "Sofortiges Neustarten des Rechners"

#: ../config/chroot_local-includes/usr/share/applications/tails-shutdown.desktop.in.h:2
msgid "Immediately shut down computer"
msgstr "Sofortiges Herunterfahren des Rechners"

#: ../config/chroot_local-includes/usr/share/applications/tor-browser.desktop.in.h:1
msgid "Tor Browser"
msgstr "Tor-Browser"

#: ../config/chroot_local-includes/usr/share/applications/tor-browser.desktop.in.h:2
msgid "Anonymous Web Browser"
msgstr "Anonymer Internet-Browser"

#: ../config/chroot_local-includes/usr/share/applications/unsafe-browser.desktop.in.h:2
msgid "Browse the World Wide Web without anonymity"
msgstr "Das Internet ohne Anonymität durchsuchen"

#: ../config/chroot_local-includes/usr/share/applications/unsafe-browser.desktop.in.h:3
msgid "Unsafe Web Browser"
msgstr "Unsicherer Internet-Browser"

#: ../config/chroot_local-includes/usr/share/desktop-directories/Tails.directory.in.h:2
msgid "Tails specific tools"
msgstr "Tails-spezifische Werkzeuge"
