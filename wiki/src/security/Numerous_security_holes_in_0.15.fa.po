# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2015-07-28 09:53+0200\n"
"PO-Revision-Date: 2015-10-19 06:45+0000\n"
"Last-Translator: sprint5 <translation5@451f.org>\n"
"Language-Team: Persian <http://weblate.451f.org:8889/projects/tails/numerous_"
"security_holes_in_015/fa/>\n"
"Language: fa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 2.4-dev\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta date=\"Tue Jan 8 00:00:00 2013\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Numerous security holes in Tails 0.15\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!tag security/fixed]]\n"
msgstr "[[!tag security/fixed]]\n"

#. type: Plain text
msgid "Several security holes affect Tails 0.15."
msgstr ""

#. type: Plain text
msgid ""
"We **strongly** urge you to [[upgrade to Tails 0.16|news/version_0.16]] as "
"soon as possible in case you are still using an older version."
msgstr ""

#. type: Title =
#, no-wrap
msgid "Details\n"
msgstr "جزییات\n"

#. type: Bullet: ' - '
msgid ""
"iceweasel ([MFSA "
"2012-91](https://www.mozilla.org/security/announce/2012/mfsa2012-98.html), "
"[MFSA "
"2013-01](https://www.mozilla.org/security/announce/2013/mfsa2013-01.html), "
"[MFSA "
"2013-02](https://www.mozilla.org/security/announce/2013/mfsa2013-02.html), "
"[MFSA "
"2013-04](https://www.mozilla.org/security/announce/2013/mfsa2013-04.html), "
"[MFSA "
"2013-05](https://www.mozilla.org/security/announce/2013/mfsa2013-05.html), "
"[MFSA "
"2013-09](https://www.mozilla.org/security/announce/2013/mfsa2013-09.html), "
"[MFSA "
"2013-11](https://www.mozilla.org/security/announce/2013/mfsa2013-11.html), "
"[MFSA "
"2013-12](https://www.mozilla.org/security/announce/2013/mfsa2013-12.html), "
"[MFSA "
"2013-15](https://www.mozilla.org/security/announce/2013/mfsa2013-15.html), "
"[MFSA "
"2013-16](https://www.mozilla.org/security/announce/2013/mfsa2013-16.html), "
"[MFSA "
"2013-17](https://www.mozilla.org/security/announce/2013/mfsa2013-17.html), "
"[MFSA "
"2013-20](https://www.mozilla.org/security/announce/2013/mfsa2013-20.html))"
msgstr ""

#. type: Bullet: ' - '
msgid "perl ([[!debsa2012 2586]])"
msgstr ""

#. type: Bullet: ' - '
msgid "tiff ([[!debsa2012 2589]])"
msgstr ""

#. type: Bullet: ' - '
msgid "ghostscript ([[!debsa2012 2595]])"
msgstr ""

#. type: Bullet: ' - '
msgid "nss ([[!debsa2013 2599]])"
msgstr ""

#. type: Bullet: ' - '
msgid "cups ([[!debsa2013 2600]])"
msgstr ""

#. type: Bullet: ' - '
msgid "gnupg ([[!debsa2013 2601]])"
msgstr ""

#. type: Bullet: ' - '
msgid "libxml2 ([[!debsa2012 2580]])"
msgstr ""
