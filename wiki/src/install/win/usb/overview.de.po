# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: 2016-01-25 19:48+0100\n"
"PO-Revision-Date: 2016-02-19 21:29+0100\n"
"Last-Translator: Tails translators <tails@boum.org>\n"
"Language-Team: \n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.7\n"

#. type: Content of: outside any tag (error?)
#| msgid ""
#| "[[!meta robots=\"noindex\"]] [[!meta title=\"Install from Windows\"]] [[!"
#| "meta stylesheet=\"bootstrap\" rel=\"stylesheet\" title=\"\"]] [[!meta "
#| "stylesheet=\"inc/stylesheets/assistant\" rel=\"stylesheet\" title=\"\"]] "
#| "[[!meta stylesheet=\"inc/stylesheets/overview\" rel=\"stylesheet\" title="
#| "\"\"]] [[!meta stylesheet=\"inc/stylesheets/windows\" rel=\"stylesheet\" "
#| "title=\"\"]] [[!inline pages=\"install/inc/tails-installation-assistant."
#| "inline\" raw=\"yes\"]] [[<span class=\"back\">Back</span>|install/win]]"
msgid ""
"[[!meta title=\"Install from Windows\"]] [[!meta stylesheet=\"bootstrap\" "
"rel=\"stylesheet\" title=\"\"]] [[!meta stylesheet=\"inc/stylesheets/"
"assistant\" rel=\"stylesheet\" title=\"\"]] [[!meta stylesheet=\"inc/"
"stylesheets/overview\" rel=\"stylesheet\" title=\"\"]] [[!meta stylesheet="
"\"inc/stylesheets/windows\" rel=\"stylesheet\" title=\"\"]] [[!inline pages="
"\"install/inc/tails-installation-assistant.inline\" raw=\"yes\"]] [[<span "
"class=\"back\">Back</span>|install/win]]"
msgstr ""
"[[!meta title=\"Installation unter Windows\"]] [[!meta stylesheet=\"bootstrap"
"\" rel=\"stylesheet\" title=\"\"]] [[!meta stylesheet=\"inc/stylesheets/"
"assistant\" rel=\"stylesheet\" title=\"\"]] [[!meta stylesheet=\"inc/"
"stylesheets/overview\" rel=\"stylesheet\" title=\"\"]] [[!meta stylesheet="
"\"inc/stylesheets/windows\" rel=\"stylesheet\" title=\"\"]] [[!inline pages="
"\"install/inc/tails-installation-assistant.inline.de\" raw=\"yes\"]] [[<span "
"class=\"back\">Zurück</span>|install/win]]"

#. type: Content of: <h1>
msgid "Install from <strong>windows</strong>"
msgstr "Installation unter <strong>Windows</strong>"

#. type: Content of: outside any tag (error?)
msgid "[[!inline pages=\"install/inc/overview\" raw=\"yes\"]] [["
msgstr "[[!inline pages=\"install/inc/overview.de\" raw=\"yes\"]] [["

#. type: Content of: <div><div><h3>
msgid "Let's go!"
msgstr "Los geht's!"

#. type: Content of: outside any tag (error?)
msgid "|install/win/usb]]"
msgstr "|install/win/usb]]"
