# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2016-01-25 19:48+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Plain text
#, no-wrap
msgid ""
"<h1 class=\"windows linux mac-usb upgrade-tails\">Restart on the intermediary Tails</h1>\n"
"<h1 class=\"clone\">Restart on the other Tails</h1>\n"
"<h1 class=\"mac-dvd\">Restart on the Tails DVD</h1>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<div class=\"step-image\">\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!img install/inc/infography/plug-source-usb.png link=\"no\" class=\"install-clone mac-clone\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!img install/inc/infography/plug-other-usb.png link=\"no\" class=\"upgrade-clone\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!img install/inc/infography/restart-on-tails.png link=\"no\" class=\"debian\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!img install/inc/infography/restart-on-intermediary-tails.png link=\"no\" class=\"clone windows linux mac-usb\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!img install/inc/infography/restart-on-dvd.png link=\"no\" class=\"mac-dvd\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!img install/inc/infography/restart-on-upgrade-usb.png link=\"no\" class=\"upgrade-tails\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "</div>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<div class=\"note clone\">\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"<p>The following instructions assume that the other Tails that you are\n"
"<span class=\"install-clone debian windows linux mac\">installing</span>\n"
"<span class=\"upgrade\">upgrading</span>\n"
"from is on a USB stick. You can also\n"
"<span class=\"install-clone debian windows linux mac\">install</span>\n"
"<span class=\"upgrade\">upgrade</span>\n"
"from a Tails DVD in a similar way.</p>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"1. <p>\n"
"     <span class=\"mac-dvd\">Make sure that the DVD is inserted in the computer.</span>\n"
"     <span class=\"usb\">Shut down the computer while leaving the <span class=\"windows linux mac\">first</span> USB stick plugged in.</span>\n"
"   </p>\n"
"   <p class=\"mac-dvd clone upgrade-tails\">Shut down the computer.</p>\n"
"   <p class=\"clone\">Plug in the other Tails USB stick that you want to\n"
"   <span class=\"install-clone debian windows linux mac\">install</span>\n"
"   <span class=\"upgrade\">upgrade</span>\n"
"   from.</p>\n"
"   <p class=\"upgrade-tails\">Unplug your Tails USB stick while leaving the intermediary USB stick plugged in.</p>\n"
msgstr ""

#. type: Bullet: '1. '
msgid "Switch on the computer."
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"   <p class=\"mac-dvd\">Immediately press-and-hold the\n"
"   <span class=\"keycap\">Option</span> key (<span class=\"keycap\">Alt</span> key) until a list of possible startup\n"
"   disks appears.</p>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "   [[!img install/mac/option_key.png class=\"screenshot mac-dvd\" link=\"no\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"   <p class=\"mac-dvd\">Choose the DVD and press\n"
"   <span class=\"keycap\">Enter</span>. The DVD\n"
"   might be labelled <span class=\"guilabel\">Windows</span> like in the following screenshot:</p>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "   [[!img install/inc/screenshots/mac_startup_dvd.png class=\"screenshot mac-dvd\" link=\"no\" alt=\"Screen with the logo of an internal hard disk labelled 'Macintosh HD' and a DVD labelled 'Windows' (selected)\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "   <div class=\"mac-usb mac-clone\">\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "   [[!inline pages=\"install/inc/steps/mac_startup_disks.inline\" raw=\"yes\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "   </div>\n"
msgstr ""

#. type: Bullet: '1. '
msgid ""
"If the computer starts on Tails, the <span class=\"guilabel\">Boot Tails</"
"span> menu appears. Choose <span class=\"guilabel\">Live</span> and press "
"<span class=\"keycap\">Enter</span>."
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"   <div class=\"note install-clone debian expert windows linux\">\n"
"   <p>Most computers do not start on Tails by default. If it does you\n"
"   are lucky. Otherwise, if the computer starts on\n"
"   <span class=\"debian expert\">Debian or Ubuntu,</span>\n"
"   <span class=\"windows\">Windows,</span>\n"
"   <span class=\"linux\">Linux,</span>\n"
"   <span class=\"install-clone\">Windows or Linux,</span>\n"
"   refer to  [[!toggle id=\"not_at_all\" text=\"the troubleshooting section\n"
"   about Tails not starting at all\"]].</p>\n"
"   </div>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "   [[!toggleable id=\"not_at_all\" text=\"\"\"\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "   <span class=\"hide\">[[!toggle id=\"not_at_all\" text=\"\"]]</span>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"   Troubleshooting\n"
"   ---------------\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "   [[!inline pages=\"install/inc/steps/not_at_all.inline\" raw=\"yes\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "     [[!img doc/first_steps/start_tails/tails_boot_menu.png class=\"screenshot\" link=\"no\" alt=\"Black screen with Tails artwork. 'Boot menu' with two options 'Live' and 'Live (failsafe)'.\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"   <div class=\"bug mac-usb mac-clone\">\n"
"   <p>If your computer fails to start on the intermediary Tails, you can\n"
"   try these other two methods:</p>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"   <ul>\n"
"     <li class=\"mac-usb\">[[Install from another operating system|os]]</li>\n"
"     <li class=\"mac-usb mac-clone\">[[Burn a DVD and then install|mac/dvd]]</li>\n"
"     <li class=\"mac-clone\">[[Install from the command line|mac/usb]]</li>\n"
"   </ul>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"   <p>If you tried them already but failed as well, then it might\n"
"   currently be impossible to start Tails on your Mac model.</p>\n"
"   </div>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"   <div class=\"bug mac-dvd\">\n"
"   <p>If your computer fails to start on the Tails DVD, then it might currently\n"
"   be impossible to start Tails on your Mac model.</p>\n"
"   </div>\n"
msgstr ""

#. type: Bullet: '1. '
msgid ""
"After 30&ndash;60 seconds, another screen called <span class=\"application"
"\">Tails Greeter</span> appears."
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"   <div class=\"note\">\n"
"   <p>If the computer stops responding or displays error messages before\n"
"   getting to <span class=\"application\">Tails Greeter</span>, refer to\n"
"   [[!toggle id=\"not_entirely\" text=\"the troubleshooting section about\n"
"   Tails not starting entirely\"]].</p>\n"
"   </div>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "   [[!toggleable id=\"not_entirely\" text=\"\"\"\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "   <span class=\"hide\">[[!toggle id=\"not_entirely\" text=\"\"]]</span>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "   [[!inline pages=\"install/inc/steps/not_entirely.inline\" raw=\"yes\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "   [[!img doc/first_steps/startup_options/tails-greeter-welcome-to-tails.png class=\"screenshot\" link=\"no\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"1. In <span class=\"application\">Tails Greeter</span>, select your preferred language in the drop-down\n"
"list on the bottom left of the screen. Click <span class=\"button\">Login</span>.\n"
msgstr ""

#. type: Bullet: '1. '
msgid "After 15&ndash;30 seconds, the Tails desktop appears."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "   [[!img install/inc/screenshots/desktop.png class=\"screenshot\" link=\"no\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<div class=\"trophy windows linux mac\">\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"<p class=\"usb\">The most difficult part is over!\n"
"Now grab your second USB stick as it is time to install the final Tails on it.</p>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"<p class=\"usb\">It is important to install the final Tails as it allows\n"
"you to store some of your documents and configuration\n"
"and benefit from automatic security upgrades.</p>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"<p class=\"clone mac-dvd\">This was a first but important step!\n"
"Now grab the <span class=\"clone\">new</span> USB stick on which you want to install Tails.</p>\n"
msgstr ""
