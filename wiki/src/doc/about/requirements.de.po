# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Tails website\n"
"POT-Creation-Date: 2015-08-10 19:28+0200\n"
"PO-Revision-Date: 2015-10-02 19:56+0100\n"
"Last-Translator: Tails translators <tails@boum.org>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: de_DE\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.6.10\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"System requirements\"]]\n"
msgstr "[[!meta title=\"Systemvoraussetzungen\"]]\n"

#. type: Plain text
msgid ""
"Tails should work on any reasonably recent computer, say manufactured after "
"2005.  Here is a detailed list of requirements:"
msgstr ""
"Tails sollte auf so gut wie jedem halbwegs aktuellen Computer laufen (d.h. "
"jünger als 10 Jahre). Folgende Voraussetzungen muss der Rechner jedoch "
"erfüllen:"

#. type: Bullet: '- '
msgid ""
"Either **an internal or external DVD reader** or the possibility to **boot "
"from a USB stick or SD card**."
msgstr ""
"Ein **internes oder externes DVD-Laufwerk**, oder die Möglichkeit von einem "
"**USB-Stick oder einer SD-Karte zu starten**."

#. type: Bullet: '- '
msgid ""
"Tails requires an <span class=\"definition\">[[!wikipedia x86]]</span> "
"compatible processor: **<span class=\"definition\">[[!wikipedia "
"IBM_PC_compatible]]</span>** and others but not <span class=\"definition"
"\">[[!wikipedia PowerPC]]</span> nor <span class=\"definition\">[[!wikipedia "
"ARM]]</span>. Mac computers are IBM PC compatible since 2006."
msgstr ""
"Tails benötigt einen Prozessor, der auf der <span class=\"definition\">[[!"
"wikipedia_de x86 desc=\"x86-Architektur\"]]</span> basiert. Deshalb läuft es "
"auf den meisten gängigen **<span class=\"definition\">[[!wikipedia_de IBM-PC-"
"kompatibler_Computer desc=\"IBM-PC-kompatiblen Computern\"]]</span>** (z. B. "
"Windows-PCs), aber nicht auf <span class=\"definition\">[[!wikipedia_de "
"PowerPC]]</span>- oder <span class=\"definition\">[[!wikipedia_de ARM-"
"Architektur desc=\"ARM\"]]</span>-Rechnern. Mac-Computer sind seit 2006 auch "
"kompatibel zu IBM-PCs."

#. type: Bullet: '- '
msgid ""
"**2 GB of RAM** to work smoothly. Tails is known to work with less memory "
"but you might experience strange behaviours or crashes."
msgstr ""
"**2GB RAM**, um sauber zu arbeiten. Notfalls läuft es auch mit weniger "
"Arbeitsspeicher, allerdings kann es dann zu unerwarteten Störungen oder "
"Systemabstürzen kommen."
